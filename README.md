# CDT-Templates

- by [Laboratorio de Ingeniería de Software Avanzada](https://www.linkedin.com/company/lisa-mx/)

This project are two Templates in LaTeX for documenting software analysis and design for schoolar or small projects. The templates include special commands for use cases, business rules, model description, etc. Also include an example of an analysis and design.

What it contain?
----------------
The template contains some commands for write Use Cases specifications and Business Rules very quickly and with elegant results. The main purpose is exploit the advantages of using LaTeX2e versus other WYSIWYG options:

1. Avoid the distraction of edit the elements of appearance and layout of the document. With this template the analyst can concentrate only on his analysis.
 
2. Delete the need of using WYSIWYG software. To work in your software specification you only will need a text editor.

3. Improve the form to work with cross references. In latex is more easy detect broken references.

4. The analysts can work collaboratively with platforms like Git. 


Install on your machine
-----------------------
<<<<<<< HEAD
1. [Download and unzip.](https://gitlab.com/ulises-velez/cdt-templates/archive/analysis.zip)
=======
1. Download and unzip or clone project.
>>>>>>> e0efee7182467d5e079acca566177c5b0754dbae

2. Open the file example.tex and compile it with LaTeX.

3. Read the files .tex in the folder and edit its content.

4. To add use cases to your document you must create copies of the archive in the folder called "cu" and incude it in your document.


Supporting Articles
-------------------
- Coming soon (o.o)b.


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.0 


![Caso de uso](https://gitlab.com/ulises.velez/cdt-templates/raw/master/Screenshots/useCase.png)
